# Welcome! 👨🏽‍💻
In this repository I submit my assignments for the Data & Information course.

Submissions for each assignment can be found in the branches. 🌴